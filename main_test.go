package main

import (
	"fmt"
	"os"
	"testing"
)

func TestAnalyzeCommits(t *testing.T) {
	app := createApp()

	_ = os.Setenv("CI_COMMIT_REF_NAME", "fix-seq-increment")
	_ = os.Setenv("CI_PROJECT_PATH", "george.kimberley/go-semrel-gitlab")
	_ = os.Setenv("CI_COMMIT_SHA", "5a3cacf07615f9feb42c1779090e4a9af4ef2778")
	_ = os.Setenv("GSG_RELEASE_BRANCHES", "master")
	_ = os.Setenv("CI_COMMIT_REF_SLUG", "fix-seq-increment")
	_ = os.Setenv("GL_TOKEN", "")
	args := os.Args[0:1]
	args = append(args, "--ci-commit-tag", "v0.25.1a+1.fix-seq-increment", "add-download-link", "-n", "release", "-u", "https://fake.com", "-d", "fake description") // Append a flag
	err := app.Run(args)
	fmt.Print(err)

}