package render

import (
	"go-semrel-gitlab/pkg/semver"
	"os"
	"testing"
	"text/template"
)

func TestBump(t *testing.T) {
	tag, err := BumpMessage("t", "tag is {{tag}}")
	if err != nil {
		t.Error(err)
	}
	if tag != "tag is t" {
		t.Fail()
	}
}

func TestRender(t *testing.T) {
	fns := template.FuncMap{
		"env":      os.Getenv,
	}

	preTs := []string{
		`{{ pref }}`,
		`{{ env }}`,
		`{{ seq }}`,
	}

	preIDs := make([]semver.PRVersion, 0)


	for _, preT := range preTs {
		fns["pref"] = getPrefixFun("a")
		fns["env"] = getPrefixFun("feat-something")
		fns["seq"] = getPrefixFun("1")
		preStr, _ := render(nil, preT, fns)
		preID, _ := semver.NewPRVersion(preStr)
		preIDs = append(preIDs, preID)
	}
	
}