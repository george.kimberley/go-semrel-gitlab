package semver

import (
	"fmt"
	"testing"
)

func TestParseBeta(t *testing.T) {
	branchName := "0.24.0a1+feat-test-increment"
	version, err := Parse(branchName)

	if err != nil {
		t.Error(err)
	}

	fmt.Print(version)
}

func TestParseAlpha(t *testing.T) {
	branchName := "0.24.0b1"
	version, err := Parse(branchName)

	if err != nil {
		t.Error(err)
	}

	fmt.Print(version)
}