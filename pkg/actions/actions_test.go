package actions

import (
	"encoding/base64"
	"fmt"
	"math/rand"
	"net/url"
	"strings"
	"testing"
	"time"

	gitlab "github.com/xanzy/go-gitlab"
	"go-semrel-gitlab/pkg/workflow"
)

const (
	apiURL = "http://gitlab/api/v4"
	user   = "root"
	pwd    = "rootpassword"
)

var (
	projectName string
	projectURL  string
	projectPath string
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func newClient(t *testing.T) *gitlab.Client {
	t.Helper()
	client, err := gitlab.NewBasicAuthClient(nil, apiURL, user, pwd)
	if err != nil {
		t.Fatal(err)
	}
	return client
}

func setupProject(t *testing.T) (*gitlab.Project, string) {
	t.Helper()
	client := newClient(t)
	projectName = fmt.Sprintf("project_%d", rand.Int())
	projectConf, _, err := client.Projects.CreateProject(
		&gitlab.CreateProjectOptions{
			Name: &projectName,
		},
	)
	if err != nil {
		t.Fatal(err)
	}
	branch := "master"
	message := "fix: initial commit"
	commit, _, err := client.Commits.CreateCommit(projectConf.PathWithNamespace, &gitlab.CreateCommitOptions{
		Branch:        &branch,
		CommitMessage: &message,
		Actions: []*gitlab.CommitAction{
			{
				Action:   gitlab.FileCreate,
				FilePath: "README.md",
				Encoding: "base64",
				Content:  base64.StdEncoding.EncodeToString([]byte("# NEW")),
			},
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	return projectConf, commit.ID
}

func tagExits(t *testing.T, project *gitlab.Project, tag string) {
	t.Helper()
	client := newClient(t)
	tagObj, _, err := client.Tags.GetTag(project.ID, tag, nil)
	if err != nil {
		t.Fatal(err)
	}
	if tagObj.Name != tag {
		t.Fatal("tag not found")
	}
}

func tagExitsWithText(t *testing.T, project *gitlab.Project, tag string, text string) {
	t.Helper()
	client := newClient(t)
	tagObj, _, err := client.Tags.GetTag(project.ID, tag, nil)
	if err != nil {
		t.Fatal(err)
	}
	if tagObj.Name != tag {
		t.Fatal("tag not found")
	}
	if !strings.Contains(tagObj.Release.Description, text) {
		t.Fatal("text not found")
	}
}

func teardownProject(t *testing.T, project *gitlab.Project) {
	t.Helper()
	client := newClient(t)
	_, err := client.Projects.DeleteProject(project.ID)
	if err != nil {
		t.Fatal(err)
	}
}

func TestTagAction(t *testing.T) {
	project, sha := setupProject(t)
	client := newClient(t)
	action := NewCreateTag(client, project.PathWithNamespace, FuncOfString(sha), "newtag", "note", false)
	aErr := workflow.Apply([]workflow.Action{action})
	if aErr != nil {
		t.Fatal(aErr)
	}
	if action.tag.Name != "newtag" {
		t.Errorf("getTagAction tag name doesn't match: %s != newtag", action.tag.Name)
	}
	tagExits(t, project, "newtag")
	teardownProject(t, project)
}

func TestGetTagAction(t *testing.T) {
	project, sha := setupProject(t)
	client := newClient(t)
	newTag := NewCreateTag(client, project.PathWithNamespace, FuncOfString(sha), "newtag", "note", false)
	aErr := workflow.Apply([]workflow.Action{newTag})
	if aErr != nil {
		t.Fatal(aErr)
	}
	action := NewGetTag(client, project.PathWithNamespace, "newtag")
	aErr = workflow.Apply([]workflow.Action{action})
	if aErr != nil {
		t.Fatal(aErr)
	}
	if action.tag.Name != "newtag" {
		t.Errorf("getTagAction tag name doesn't match: %s != newtag", action.tag.Name)
	}
	tagExits(t, project, "newtag")
	teardownProject(t, project)
}

func TestAddLink(t *testing.T) {
	project, sha := setupProject(t)
	client := newClient(t)
	createTag := NewCreateTag(client, project.PathWithNamespace, FuncOfString(sha), "newtag", "note", false)
	addLink := NewAddLink(&AddLinkParams{
		Client:          client,
		Project:         project.PathWithNamespace,
		LinkDescription: "Linux executable",
		MDLinkFunc:      func() string { return "[release](http://localhost)" },
		TagFunc:         createTag.TagFunc(),
	})
	aErr := workflow.Apply([]workflow.Action{createTag, addLink})
	if aErr != nil {
		t.Fatal(aErr)
	}
	action := NewGetTag(client, project.PathWithNamespace, "newtag")
	aErr = workflow.Apply([]workflow.Action{action})
	if aErr != nil {
		t.Fatal(aErr)
	}
	if action.tag.Name != "newtag" {
		t.Errorf("getTagAction tag name doesn't match: %s != newtag", action.tag.Name)
	}
	if !strings.Contains(action.tag.Release.Description, "release") {
		t.Errorf("description was not updated: %s", action.tag.Release.Description)
	}
	teardownProject(t, project)
}

func TestUpload(t *testing.T) {
	project, sha := setupProject(t)
	projectURL, err := url.Parse(project.WebURL)
	if err != nil {
		t.Fatal(err)
	}
	client := newClient(t)
	createTag := NewCreateTag(client, project.PathWithNamespace, FuncOfString(sha), "newtag", "note", false)
	upload := NewUpload(client, project.PathWithNamespace, projectURL, "actions_test.go")
	addLink := NewAddLink(&AddLinkParams{
		Client:          client,
		Project:         project.PathWithNamespace,
		LinkDescription: "Linux executable",
		MDLinkFunc:      upload.MDLinkFunc(),
		TagFunc:         createTag.TagFunc(),
	})
	aErr := workflow.Apply([]workflow.Action{createTag, upload, addLink})
	if aErr != nil {
		t.Fatal(aErr)
	}
	action := NewGetTag(client, project.PathWithNamespace, "newtag")
	aErr = workflow.Apply([]workflow.Action{action})
	if aErr != nil {
		t.Fatal(aErr)
	}
	if action.tag.Name != "newtag" {
		t.Errorf("getTagAction tag name doesn't match: %s != newtag", action.tag.Name)
	}
	if !strings.Contains(action.tag.Release.Description, "actions_test.go") {
		t.Errorf("description was not updated: %s", action.tag.Release.Description)
	}
	teardownProject(t, project)
}
