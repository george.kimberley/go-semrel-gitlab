package actions

import (
	"fmt"
	"net/url"

	"github.com/pkg/errors"
	gitlab "github.com/xanzy/go-gitlab"
	"go-semrel-gitlab/pkg/workflow"
)

// Upload ..
type Upload struct {
	client             *gitlab.Client
	project            string
	projectURL         *url.URL
	file               string
	projectFile        *gitlab.ProjectFile
	fullprojectFileURL *url.URL
}

// Do implements Action for Upload
func (action *Upload) Do() *workflow.ActionError {
	if action.projectFile != nil {
		return nil
	}
	projectFile, resp, err := action.client.Projects.UploadFile(action.project, action.file)
	if err != nil {
		retry := false
		if resp.StatusCode == 502 {
			retry = true
		}
		return workflow.NewActionError(errors.Wrap(err, "upload file"), retry)
	}
	action.projectFile = projectFile

	action.fullprojectFileURL, err = url.Parse(action.projectURL.String() + action.projectFile.URL)
	if err != nil {
		return workflow.NewActionError(errors.Wrap(err, "parse full upload url"), false)
	}

	return nil
}

// Undo implements Action for Upload
func (action *Upload) Undo() error {
	if action.projectFile == nil {
		return nil
	}
	fmt.Printf("File upload cannot be rolled back: %s.\n", action.file)
	return errors.Errorf("file upload cannot be rolled back: %s", action.file)
}

// MDLinkFunc returns an accessor function to project file markdown link
func (action *Upload) MDLinkFunc() func() string {
	return func() string {
		if action.projectFile == nil {
			return ""
		}
		return action.projectFile.Markdown
	}
}

// LinkURLFunc returns an accessor function to project file url
func (action *Upload) LinkURLFunc() func() string {
	return func() string {
		if action.fullprojectFileURL == nil {
			return ""
		}
		return action.fullprojectFileURL.String()
	}
}

// NewUpload ..
func NewUpload(client *gitlab.Client, project string, projectURL *url.URL, file string) *Upload {
	return &Upload{
		client:     client,
		project:    project,
		projectURL: projectURL,
		file:       file,
	}
}
